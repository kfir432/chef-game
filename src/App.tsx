import * as React from 'react';
import './App.css';
import './SiteCss.css';
import Home from './pages/HomePage/Home';
import {LocalStorageManager} from './pages/ChefGames/utils/LocalStorageManager';
import * as constants from './pages/ChefGames/constants';
import ChefGamesMain from './pages/ChefGames';

class App extends React.Component {
  render() {
    const chefGameActiveSeason : boolean = true;
    let chefGameDisable : boolean = LocalStorageManager.getItem(constants.CHEF_GAME_DISABLED) !== ''
      ? false
      : true;
    return (
      <div className="HomePage">
        <div className="MainImage">
          {chefGameActiveSeason && chefGameDisable
            ? <Home/>
            : <ChefGamesMain/>}
        </div>
      </div>
    );
  }
}

export default App;
