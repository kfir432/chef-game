import * as React from 'react';
import {ActionLink, ActionButton} from '../Shared';
interface IChefGamesWelcomeProps {
    imageSrc : string;

}
class ChefGamesWelcome extends React.Component < IChefGamesWelcomeProps,
any > {
    public render() {
        return (
            <div className="chef-game-welcome-wrapper">
                <div className="welcome-image-wrapper">
                    <img src={this.props.imageSrc}/>
                </div>
                <div className="welcome-text col-md-12 col-sm-12 col-xs-12">
                    <h3>לראשונה בישראל אתם יכולים להזמין הביתה</h3>
                    <h3>את המנות המדהימות של משחקי השף!</h3>
                </div>
                <div className="how-it-works col-md-12 col-sm-12 col-xs-12">
                    <ActionLink
                        compClassName="round-button black-white-btn bold"
                        handleClick={e => console.log(e)}
                        text="איך זה עובד ?"/>
                </div>
                <div className="clearfix"/>
                <div className="chef-game-actions">
                    <div className="col-md-6 col-sm-6 col-xs-12">
                        <ActionButton
                            compClassName="red-btn"
                            text="להרשמה והזמנה ממשחקי השף"
                            handleClick={e => console.log(e)}/>
                    </div>
                    <div className="col-md-6 col-sm-6 col-xs-12">
                        <ActionButton
                            compClassName="white-btn-red-border hvr-fade red-bg"
                            text="דלג להזמנה מהמסעדות שבסביבתך"
                            handleClick={e => console.log(e)}/>
                    </div>
                </div>
            </div>
        );
    }

}
export default ChefGamesWelcome;