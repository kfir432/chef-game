import * as React from 'react';
import {Button} from 'react-bootstrap';

interface IActionButtonProps {
    text : string;
    handleClick : Function;
    isLoading?: boolean;
    disabled?: boolean;
    compClassName?: string;
}

export const ActionButton : React.SFC<IActionButtonProps> = (props) => {
    const {isLoading, disabled, text, compClassName} = props;
    return (
        <Button
            className={`ActionButton ${compClassName}`}
            disabled={isLoading || disabled}
            onClick={() => props.handleClick()}>
            {isLoading
                ? 'המתן...'
                : text}
        </Button>
    );
};
// <div className="ActionButton" onClick={() => this.props.handleClick()}>
// {this.props.text} </div>