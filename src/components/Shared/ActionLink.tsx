﻿import * as React from 'react';

interface ILinkProps {
    text : string;
    handleClick : Function;
    inactive?: boolean;
    compClassName?: string;

}
export const ActionLink : React.SFC<ILinkProps>= (props) => {
    const {
        compClassName = "",
        inactive,
        text
    } = props;
    return (
        <span
            className={(compClassName
            ? compClassName
            : "") + (inactive
            ? " ActionLink Inactive"
            : " ActionLink")}
            onClick={props.handleClick()}>
            {text}
        </span>
    );
};
