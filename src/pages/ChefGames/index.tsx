import * as React from 'react';
import * as constants from './constants';
import {LocalStorageManager} from './utils/LocalStorageManager';
import '../../sass/chef-styles.css';
import ChefGamesWelcome from '../../components/ChefGames/ChefGamesWelcome';

class ChefGamesMain extends React.Component {
    componentWillMount() {
        // Set localStorage param to prevent second appering !
        LocalStorageManager.setItem(constants.CHEF_GAME_DISABLED, "true");
    }
    public render() {
        return (
            <div id="chef-game">
                <div className="jumbotron">
                    <ChefGamesWelcome imageSrc="./img/chef.jpg"/>
                </div>
            </div>

        );
    }
}
export default ChefGamesMain;