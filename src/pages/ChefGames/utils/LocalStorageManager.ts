interface ILocalStorageManager {
    key : string;
    data : string;
    // setItem : (key : string, data : string) => void; getItem : (key : string) =>
    // string; removeItem : (key : string) => void;
}

class LocalStorageManager implements ILocalStorageManager {
    key : string;
    data : string;

    public static setItem(key : string, data : string) {
        localStorage.setItem(key, data);
    }
    public static getItem(key : string) : string {
        let value = localStorage.getItem(key);
        let item = value == null
            ? ''
            : value;
        return item;
    }
    public static removeItem(key : string) {
        localStorage.removeItem(key);
    }

}
export {LocalStorageManager};